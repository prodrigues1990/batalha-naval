import pickle

# Retorna um novo dicionário de jogo
def new_game():
    pass

# Retorna True se existir um jogador com o nome indicado
def has_player(game, name):
    pass

# Adiciona um jogador com o nome indicado
def add_player(game, name):
    pass

# Retorna true se o jogador participar no jogo em curso
def player_in_match(game, name):
    pass

# Remove o jogador com o nome indicado
def remove_player(game, name):
    pass

# Retorna True se existirem jogadores registados
def has_players(game):
    pass

# Retorna um dicionário de jogadores com a seguinte estrutura:
# {
#     'name': str,
#     'matches': int,
#     'wins': int
# }
def get_players(game):
    pass

# Retorna True se existir um jogo corrente
def has_match(game):
    pass

# Inicia um novo jogo entre os jogadores com os nomes indicados
def start_match(game, player_1_name, player_2_name):
    pass

# Coloca o navio do tipo indicado, na posição indicada, no tabuleiro de navios
# do jogador com o nome indicado
# A orientação é opcional (só se aplica em navios com tamanho 1)
def place_ship(game, player_name, ship_type, board_line, board_column, orientation=None):
    pass

# Retorna True se a posição indicada é válida para a colocação de um navio
def is_valid_position(game, player_name, ship_type, board_line, board_column, orientation):
    pass

# Retorna True se o jogador indicado tiver navios do tipo indicado disopíveis para colocação
def is_ship_type_available(game, player_name, ship_type):
    pass

# Retorna True se o jogador indicado tiver um navio colocado na posição indicada
def is_ship_in_position(game, player_name, board_line, board_column):
    pass

# Remove o navio que ocupa a posição indicada no tabuleiro do jogador indicado
def remove_ship(game, player_name, board_line, board_column):
    pass

# Retorna True se os dois jogadores do jogo corrente tiverem todos os navios colocados
def all_ships_placed(game):
    pass

# Retorna True se o jogo corrente tiver o combate iniciado
def has_combat(game):
    pass

# Inicia o combate no jogo corrente
def start_combat(game):
    pass

# Retorna true se o jogador com o nome indicado participar no jogo corrente
def in_match(game, name):
    pass

# Provoca a desistência de um ou dois jogadores, de acordo com os nomes indicados
def withdraw(game, name, second_name=None):
    pass

# Retorna True se a posição de tiro indicada for legal
def is_valid_shot(game, board_line, board_column):
    pass

# Executa um tiro numa posição indicada, no tabuleiro do jogador contrário ao jogador 
# com o nome indicado
def shot(game, player_name, board_line, board_column):
    pass

# Retorna uma lista de dicionários com informação sobre o jogo corrente.
# A lista tem dois elementos: um por cada jogador. O formato é o seguinte:
# [
#     {
#         'name': str,
#         'total_shots': int,
#         'shots_on_ships': int,
#         'sunk_ships': int
#     }
# ]
def get_match_state(game):
    pass

def save(game, filename):
    pass

def load(filename):
    pass

## Funções auxiliares

# Ordena uma lista de dicionários de acordo com a chave indicada.
def __sort(dict_list, sort_key, data_type=str):
    for i in range(len(dict_list)):
        for j in range(len(dict_list)-i-1):
            if __get_dict_value(dict_list[j], sort_key) > __get_dict_value(dict_list[j+1], sort_key):
                tmp = dict_list[j]
                dict_list[j] = dict_list[j+1]
                dict_list[j+1] = tmp
    return dict_list