# Batalha Naval

Este projecto foi entregue aos alunos de Engenharia Informática da Universidade Autónoma de Lisboa como introdução a programação.

Consiste em implementar um jogo de batalha naval executado em linha de comandos, com a linguagem python.

Uma descrição funcional do programa está disponível no PDF em anexo.

## Objectivos

Implementar as funções definidas em `battleship_game.py`.

__O ficheiro `program.py` não deverá ser alterado.`

